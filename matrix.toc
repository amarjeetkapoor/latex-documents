\contentsline {chapter}{\numberline {1}Matrix}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Matrix Operations}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Multiplication of a Matrix by a Scalar}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Addition of a Matrix}{3}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Multiplications of Matrix}{4}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Transpose of Matrix}{6}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Symmetric Matrix}{8}{subsection.1.2.5}
\contentsline {subsection}{\numberline {1.2.6}Unit Matrix}{8}{subsection.1.2.6}
\contentsline {subsection}{\numberline {1.2.7}Inverse of a Matrix}{9}{subsection.1.2.7}
\contentsline {subsection}{\numberline {1.2.8}Orthogonal Matrix}{9}{subsection.1.2.8}
\contentsline {subsection}{\numberline {1.2.9}Differentiating a Matrix}{12}{subsection.1.2.9}
\contentsline {subsection}{\numberline {1.2.10}Integrating a Matrix}{14}{subsection.1.2.10}
\contentsline {section}{\numberline {1.3}Cofactor or Adjoint Method to Determine the inverse of a Matrix}{15}{section.1.3}
\contentsline {section}{\numberline {1.4}Inverse of a Matrix by Row Reduction}{19}{section.1.4}
